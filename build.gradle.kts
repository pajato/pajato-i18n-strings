plugins {
    alias(libs.plugins.kmp.lib)
}

group = "com.pajato"
version = "0.10.3"
description = "The Pajato localized strings feature, KMP common target project"

kotlin.sourceSets {
    val commonMain by getting { dependencies { implementation(libs.kotlinx.serialization.json) } }

    val commonTest by getting {
        dependencies {
            implementation(libs.kotlin.test)
            implementation(libs.pajato.test)
        }
    }
}
