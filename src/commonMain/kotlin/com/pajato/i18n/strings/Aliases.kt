package com.pajato.i18n.strings

public typealias Key = String
public typealias Value = String
public typealias Arg = Pair<Key, Value>
