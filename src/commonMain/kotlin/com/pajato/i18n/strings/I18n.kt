package com.pajato.i18n.strings

import java.util.Locale

// Use the Victor Kropp project eventually: name.kropp.kotlinx.gettext! Defer the setup for expediency

public interface I18n {

    /** Current locale. */
    public val locale: Locale

    /** Get translated, stored text using a short key. */
    public fun get(key: Key): String

    /** Get translated, formatted, stored text using a short key and provided [args]. */
    public fun get(key: Key, vararg args: Pair<String, String>): String

    /** Get translated, stored text using a short key for different [plural] forms, based on a provided number [n]. */
    public fun getPlural(key: Key, plural: String?, n: Int): String

    /** Get translates, stored text using a short key with different [plural] forms, based on provided number [n]. */
    public fun getPlural(key: Key, plural: String?, n: Long): String

    /** Get translated, formatted, stored text using a short key with different [plural] forms, based on a provided
     * number [n], and with provided [args]. */
    public fun getPlural(key: Key, plural: String?, n: Int, vararg args: Arg): String

    /** Get translated, formatted, stored text using a short key with different [plural] forms, based on provided
     *  number [n] and with provided [args]. */
    public fun getPlural(key: Key, plural: String?, n: Long, vararg args: Arg): String

    /** Get translated, stored text using a short key in a given [context]. */
    public fun getWithContext(context: String, key: Key): String

    /** Get translated, formatted stored text using a short key with provided [args] in a given [context]. */
    public fun getWithContext(context: String, key: Key, vararg args: Arg): String

    /** Get translated, stored text using a short key, with different [plural] forms, based on a provided number [n]
     * in a given [context]. */
    public fun getPluralWithContext(context: String, key: Key, plural: String?, n: Int): String

    /** Get translated, stored text using a short key, with different [plural] forms, based on a provided number [n]
     * in a given [context]. */
    public fun getPluralWithContext(context: String, key: Key, plural: String?, n: Long): String

    /** Get translated, formatted, stored text using a short key with different [plural] forms and with provided
     * [args], based on a provided number [n] in a given [context]. */
    public fun getPluralWithContext(context: String, key: Key, plural: String?, n: Int, vararg args: Arg): String

    /** Get translated, formatted stored text using a short key with different [plural] forms and with provided
     * [args], based on a provided number [n] in a given [context]. */
    public fun getPluralWithContext(context: String, key: Key, plural: String?, n: Long, vararg args: Arg): String

    /** Mark stored text for translation, but do not translate it. */
    public fun markGet(key: Key): String
}
