package com.pajato.i18n.strings

import com.pajato.i18n.strings.StringsResource.put

public object I18nStrings {
    public const val STRINGS_REGISTERED_KEY: String = "StringsRegisteredKey"

    internal fun registerStrings() { put(STRINGS_REGISTERED_KEY, "Key: '{{key}}' is already registered!") }
}
