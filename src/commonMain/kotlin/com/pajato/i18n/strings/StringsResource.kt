package com.pajato.i18n.strings

import com.pajato.i18n.strings.I18nStrings.STRINGS_REGISTERED_KEY
import java.util.Locale

public object StringsResource : I18n {
    public val cache: MutableMap<Key, Value> = mutableMapOf()

    public fun putOrThrow(key: Key, value: Value) {
        if (cache.contains(key).not()) cache[key] = value else throwError(key)
    }

    public fun put(key: Key, value: Value) { cache[key] = value }

    override val locale: Locale get() = Locale.getDefault()

    override fun get(key: Key): String = cache[key] ?: "Unregistered key: $key!"

    override fun get(key: Key, vararg args: Arg): String = get(key).format(args)

    override fun getPlural(key: Key, plural: String?, n: Int): String { TODO("Not yet implemented") }

    override fun getPlural(key: Key, plural: String?, n: Long): String { TODO("Not yet implemented") }

    override fun getPlural(key: Key, plural: String?, n: Int, vararg args: Arg): String {
        TODO("Not yet implemented")
    }

    override fun getPlural(key: Key, plural: String?, n: Long, vararg args: Arg): String {
        TODO("Not yet implemented")
    }

    override fun getWithContext(context: String, key: Key): String { TODO("Not yet implemented") }

    override fun getWithContext(context: String, key: Key, vararg args: Arg): String {
        TODO("Not yet implemented")
    }

    override fun getPluralWithContext(context: String, key: Key, plural: String?, n: Int): String {
        TODO("Not yet implemented")
    }

    override fun getPluralWithContext(context: String, key: Key, plural: String?, n: Long): String {
        TODO("Not yet implemented")
    }

    override fun getPluralWithContext(context: String, key: Key, plural: String?, n: Int, vararg args: Arg): String {
        TODO("Not yet implemented")
    }

    override fun getPluralWithContext(context: String, key: Key, plural: String?, n: Long, vararg args: Arg): String {
        TODO("Not yet implemented")
    }

    override fun markGet(key: Key): String = get(key)

    private fun throwError(key: Key) {
        I18nStrings.registerStrings()
        throw StringsResourceError(get(STRINGS_REGISTERED_KEY, Arg("key", key)))
    }

    private fun String.format(args: Array<out Pair<String, String>>): String {
        var currentOffset = 0
        var nextIndex = indexOf("{{", currentOffset)
        if (nextIndex == -1) return this

        return buildString {
            do {
                append(this@format.substring(currentOffset, nextIndex))
                currentOffset = nextIndex + 2
                nextIndex = this@format.indexOf("}}", currentOffset)
                if (nextIndex == -1) break // incorrect format, append the rest of the string as is

                val key = this@format.substring(currentOffset, nextIndex)
                for (arg in args) {
                    if (key == arg.first) {
                        append(arg.second)
                        break
                    }
                }
                currentOffset = nextIndex + 2
                nextIndex = this@format.indexOf("{{", currentOffset)
            } while (nextIndex != -1)

            if (currentOffset < this@format.length) {
                append(this@format.substring(currentOffset, this@format.length))
            }
        }
    }
}
