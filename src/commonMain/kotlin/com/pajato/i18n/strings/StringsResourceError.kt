package com.pajato.i18n.strings

public class StringsResourceError(message: String) : Exception(message)
