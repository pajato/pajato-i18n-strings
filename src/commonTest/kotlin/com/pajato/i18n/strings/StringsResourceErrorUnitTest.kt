package com.pajato.i18n.strings

import com.pajato.test.ReportingTestProfiler
import kotlin.test.Test
import kotlin.test.assertEquals
import kotlin.test.assertFailsWith

class StringsResourceErrorUnitTest : ReportingTestProfiler() {
    @Test fun `When generating a strings resource error, verify the exception`() {
        val excMessage = "some useful message"
        assertFailsWith<StringsResourceError> { throw StringsResourceError(excMessage) }
            .also { assertEquals(excMessage, it.message) }
    }
}
