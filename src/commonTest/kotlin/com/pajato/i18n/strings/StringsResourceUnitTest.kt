package com.pajato.i18n.strings

import com.pajato.test.ReportingTestProfiler
import java.util.Locale
import kotlin.test.BeforeTest
import kotlin.test.Test
import kotlin.test.assertEquals
import kotlin.test.assertFailsWith

class StringsResourceUnitTest : ReportingTestProfiler() {

    @BeforeTest fun setUp() {
        StringsResource.cache.clear()
    }

    @Test fun `When inserting a string into an empty map, verify the map size`() {
        StringsResource.putOrThrow("test", "A string to test")
        assertEquals(1, StringsResource.cache.size)
    }

    @Test fun `When re-inserting a string into a map, verify an exception`() {
        val key = "test"
        val expected = "A string to test"
        StringsResource.putOrThrow(key, expected)
        assertFailsWith<StringsResourceError> { StringsResource.putOrThrow(key, expected) }
            .also { assertEquals("Key: 'test' is already registered!", it.message) }
    }

    @Test fun getLocale() { assertEquals(Locale.getDefault(), StringsResource.locale) }

    @Test fun `When accessing an unregistered string, verify result`() {
        val key = "xyzzy"
        assertEquals("Unregistered key: $key!", StringsResource.get(key))
    }

    @Test fun `When accessing a registered string, verify the result`() {
        val key = "registered_string"
        val expected = "A string registered using the key: $key"
        StringsResource.putOrThrow(key, expected)
        assertEquals(expected, StringsResource.get(key))
    }

    @Test fun `When accessing a null value using arguments, verify correct null result`() {
        val key = "unregistered_string"
        assertEquals("Unregistered key: $key!", StringsResource.get(key, Arg("xyzzy", "12")))
    }

    @Test fun `When accessing a value using non-existing arguments, verify correct result`() {
        val key = "key"
        val expected = "Text without substitution templates"
        StringsResource.putOrThrow(key, expected)
        assertEquals(expected, StringsResource.get(key, Arg("xyzzy", "12")))
    }

    @Test fun `When accessing a value using arguments, verify correct substituted result`() {
        val key = "key"
        val expected = "Result with 12 entries has the expected value 12 and that is it!"
        StringsResource.putOrThrow(key, "Result with {{xyzzy}} entries has the expected value {{xyzzy}} and that is it!")
        assertEquals(expected, StringsResource.get(key, Arg("xyzzy", "12")))
    }

    @Test fun `When accessing a single thing with an integer count, verify pluralization`() {
        assertFailsWith<NotImplementedError> { StringsResource.getPlural("key", "noun", 1) }
    }

    @Test fun `When accessing two items with arguments and an int count, verify pluralization`() {
        assertFailsWith<NotImplementedError> {
            StringsResource.getPlural("key", "{{arg}} horses", 2, Pair("arg", "2"))
        }
    }

    @Test fun `When accessing a single thing with a long count, verify pluralization`() {
        assertFailsWith<NotImplementedError> { StringsResource.getPlural("key", "noun", 1L) }
    }

    @Test fun `When accessing two items with arguments and a long count, verify pluralization`() {
        assertFailsWith<NotImplementedError> {
            StringsResource.getPlural("key", "{{2}} horses", 2L, Pair("arg", "2"))
        }
    }

    @Test fun getWithSimpleContext() {
        assertFailsWith<NotImplementedError> { StringsResource.getWithContext("context", "key") }
    }

    @Test fun getWithArgumentsAndContext() {
        assertFailsWith<NotImplementedError> {
            StringsResource.getWithContext("context", "key", Arg("key", "value"))
        }
    }

    @Test fun getPluralWithContextUsingInt() {
        assertFailsWith<NotImplementedError> {
            StringsResource.getPluralWithContext("context", "key", "noun", 1)
        }
    }

    @Test fun getPluralWithContextUsingLong() {
        assertFailsWith<NotImplementedError> {
            StringsResource.getPluralWithContext("context", "key", "nouns", 2L)
        }
    }

    @Test fun getPluralWithContextAndArgumentsUsingInt() {
        assertFailsWith<NotImplementedError> {
            val arg = Arg("key", "value")
            StringsResource.getPluralWithContext("context", "key", "plural", 1, arg)
        }
    }

    @Test fun getPluralWithContextAndArgumentsUsingLong() {
        assertFailsWith<NotImplementedError> {
            val arg = Arg("key", "value")
            StringsResource.getPluralWithContext("context", "key", "plurals", 2L, arg)
        }
    }

    @Test fun `When accessing an untranslated string, verify a correct result`() {
        val key = "test"
        val expected = "A string to test"
        StringsResource.putOrThrow(key, expected)
        assertEquals(expected, StringsResource.markGet(key))
    }
}
